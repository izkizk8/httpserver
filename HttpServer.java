package com.izkizk8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpServer {

    public static void main(String[] args) {
        try {

            /*监听端口号，只要是8888就能接收到*/
            ServerSocket ss = new ServerSocket(8888);

            while (true) {
                /*实例化客户端，固定套路，通过服务端接受的对象，生成相应的客户端实例*/
                Socket socket = ss.accept();
                /*获取客户端输入流，就是请求过来的基本信息：请求头，换行符，请求体*/
                BufferedReader bd = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                /*状态码*/
                int status = 200;
                /*状态描述*/
                String description = "OK";
                /*响应体*/
                String body = "";

                /**
                 * 接受HTTP请求，并解析数据
                 */
                String requestHeader;
                int contentLength = 0;

                if ((requestHeader = bd.readLine()) != null && !requestHeader.isEmpty()) {
                    System.out.println(requestHeader);
                    /**
                     * 获得GET参数
                     * /add?a=1&b=2
                     * /cut?a=1&b=2
                     * /mul?a=1&b=2
                     * /div?a=1&b=2
                     */
                    if (requestHeader.startsWith("GET")) {
                        int begin = requestHeader.indexOf("GET /") + "GET /".length();
                        int end = requestHeader.indexOf(" HTTP");
                        String action = requestHeader.substring(begin, begin + 3);
                        begin = begin + 4;
                        String[] parameters = requestHeader.substring(begin, end).split("&");
                        if ("add".equals(action)) {
                            double a = Integer.parseInt(parameters[0].substring(2));
                            double b = Integer.parseInt(parameters[1].substring(2));
                            double result = a + b;
                            body = a + " + " + b + " = " + result;
                        } else if ("cut".equals(action)) {
                            double a = Integer.parseInt(parameters[0].substring(2));
                            double b = Integer.parseInt(parameters[1].substring(2));
                            double result = a - b;
                            body = a + " - " + b + " = " + result;
                        } else if ("mul".equals(action)) {
                            double a = Integer.parseInt(parameters[0].substring(2));
                            double b = Integer.parseInt(parameters[1].substring(2));
                            double result = a * b;
                            body = a + " * " + b + " = " + result;
                        } else if ("div".equals(action)) {
                            double a = Integer.parseInt(parameters[0].substring(2));
                            double b = Integer.parseInt(parameters[1].substring(2));
                            double result = a / b;
                            body = a + " / " + b + " = " + result;
                        } else {
                            /**
                             * 错误GET请求，返回400 Bad request
                             */
                            status = 400;
                            description = "Bad request";
                        }
                    } else {
                        /**
                         * 非GET请求，返405 Method not allowed
                         */
                        status = 405;
                        description = "Method not allowed";
                    }
                }

                /*发送回执*/
                PrintWriter pw = new PrintWriter(socket.getOutputStream());

                pw.println("HTTP/1.1 " + status + " " + description);
                pw.println("Content-type:text/html");
                pw.println();
                pw.println(body);

                pw.flush();
                socket.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
